module.exports = {
  tutorialSidebar: {
    'Tutorial Basics': ['tutorial-basics/congratulations', 'tutorial-basics/create-a-blog-post', 'tutorial-basics/create-a-document', 'tutorial-basics/create-a-page'],
    'Intro': ['intro']
    
  },
  apiSidebar: ['cliss/cli', 'cliss/functions', 'cliss/metodos'],
};